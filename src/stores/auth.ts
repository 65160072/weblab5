import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
    const currentUser = ref<User>({
    id: 0,
    email: 'mana123@gmail.com',
    password: 'Pass@1234',
    fullname: 'มานะ งานดี',
    gender: 'male',
    roles: ['user']
})

  return { currentUser }
})

